module bitbucket.org/uwaplmlf2/go-mlf2

require (
	bitbucket.org/uwaplmlf2/couchdb v0.7.1
	bitbucket.org/uwaplmlf2/mlf2sexp v0.3.0
	github.com/BurntSushi/toml v0.3.1
	github.com/ChimeraCoder/anaconda v2.0.0+incompatible
	github.com/ChimeraCoder/tokenbucket v0.0.0-20131201223612-c5a927568de7 // indirect
	github.com/azr/backoff v0.0.0-20160115115103-53511d3c7330 // indirect
	github.com/batchatco/go-native-netcdf v0.0.0-20200607201726-cbf676ee1a31
	github.com/dsnet/golib v0.0.0-20171103203638-1ea166775780
	github.com/dustin/go-jsonpointer v0.0.0-20160814072949-ba0abeacc3dc // indirect
	github.com/dustin/gojson v0.0.0-20160307161227-2e71ec9dd5ad // indirect
	github.com/garyburd/go-oauth v0.0.0-20180319155456-bca2e7f09a17 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/satori/go.uuid v1.2.0
	github.com/streadway/amqp v0.0.0-20190312223743-14f78b41ce6d
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	golang.org/x/sys v0.0.0-20190624142023-c5567b49c5d0 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20191108220845-16a3f7862a1a // indirect
	google.golang.org/grpc v1.21.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

go 1.13
