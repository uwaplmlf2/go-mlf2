#
# Build a SquashFS file system images containing all of the mlf2data services. This
# makefile must be run on a Linux system.
#
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)
progs := dbstore msgmon mlf2resp mlf2slack
image = mlf2data_$(VERSION).raw

all: $(image)

$(image): $(progs) os-release
	rm -rf build $(image)
	mkdir -p build/usr/bin build/usr/lib build/etc build/proc build/sys build/dev build/run build/tmp build/var/tmp build/var/lib/mlf2data build/usr/share/ca-certificates
	cp -v $(progs) build/usr/bin
	cp -v os-release build/usr/lib
	touch build/etc/resolv.conf build/etc/machine-id
	for d in config systemd; do \
	    $(MAKE) DESTDIR=$(CURDIR)/build -C $$d install;\
	done
	mksquashfs build/ $(image)

.version:
	@echo "$(VERSION)" > .version

.PHONY: .version
