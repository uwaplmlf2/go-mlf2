// Package rpc implements a gRPC server to move messages from the database
// to the AMQP broker.
package rpc

import (
	fmt "fmt"
	"log"
	"sync"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/messages"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	context "golang.org/x/net/context"
)

type QueueServer struct {
	conn *amqp.Connection
	db   *couchdb.Database
	mu   sync.Mutex
}

type exchange struct {
	channel *amqp.Channel
	name    string
}

func newExchange(conn *amqp.Connection, name string) (*exchange, error) {
	var err error
	ex := exchange{}
	ex.channel, err = conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "open channel")
	}

	ex.name = name
	if err = ex.channel.ExchangeDeclare(
		ex.name, // name of the exchange
		"topic", // type
		true,    // durable
		false,   // delete when complete
		false,   // internal
		false,   // noWait
		nil,     // arguments
	); err != nil {
		return nil, errors.Wrap(err, "exchange declare")
	}

	return &ex, nil
}

func (e *exchange) publishMessage(qname string, msg amqp.Publishing) error {
	return nil
}

func (e *exchange) close() {
	e.channel.Close()
}

func (e *exchange) publishCommands(ctx context.Context, m *messages.Message) (int, error) {
	headers := make(amqp.Table)
	headers["floatid"] = int32(m.Floatid)
	headers["db_id"] = m.ID
	headers["comment"] = m.Comment

	qname := fmt.Sprintf("float-%d.commands", m.Floatid)
	n := m.Count()
	queued := int(0)
	for i, cmd := range m.Commands() {
		if i == (n - 1) {
			headers["eot"] = "yes"
		}
		msg := amqp.Publishing{
			Headers:      headers,
			ContentType:  "text/plain",
			DeliveryMode: 2,
			ReplyTo:      m.Sender,
			Timestamp:    time.Now(),
			Body:         []byte(cmd.String()),
		}
		if err := e.publishMessage(qname, msg); err != nil {
			return queued, errors.Wrap(err, "publishCommands")
		}
		queued++
	}

	return queued, nil
}

func (e *exchange) cancelCommands(ctx context.Context, m *messages.Message) error {
	qname := fmt.Sprintf("float-%d.cancellation", m.Floatid)
	text := fmt.Sprintf("[%q]", m.ID)
	msg := amqp.Publishing{
		ContentType:  "application/json",
		DeliveryMode: 2,
		ReplyTo:      m.Sender,
		Timestamp:    time.Now(),
		Body:         []byte(text),
	}

	return e.publishMessage(qname, msg)
}

// NewServer returns a new QueueServer
func NewServer(conn *amqp.Connection, db *couchdb.Database) *QueueServer {
	s := QueueServer{
		conn: conn,
		db:   db,
	}
	return &s
}

// Queue adds the contents of one or more messages to the outgoing queue for
// an MLF2 float.
func (s *QueueServer) Queue(ctx context.Context, req *QueueRequest) (*QueueResponse, error) {
	ex, err := newExchange(s.conn, "commands")
	if err != nil {
		return nil, err
	}
	defer ex.close()

	msgs := s.getMessages(ctx, req.Ids)
	resp := QueueResponse{}
	for _, m := range msgs {
		if m.State != "ready" {
			continue
		}
		n, err := ex.publishCommands(ctx, &m)
		if err != nil {
			return &resp, err
		}
		resp.Commands += int32(n)

		m.State = "queued"
		m.Queued = messages.Time{time.Now().UTC()}
		err = s.putMessage(ctx, &m)
		if err != nil {
			return &resp, err
		}
		resp.Ids = append(resp.Ids, m.ID)
	}

	return &resp, nil
}

func (s *QueueServer) getMessages(ctx context.Context, ids []string) []messages.Message {
	ms := make([]messages.Message, 0, len(ids))
	s.mu.Lock()
	defer s.mu.Unlock()
	for _, id := range ids {
		m := messages.Message{}
		err := s.db.Get(id, &m)
		if err != nil {
			log.Printf("Error fetching document %q", id)
		}
	}
	return ms
}

func (s *QueueServer) putMessage(ctx context.Context, m *messages.Message) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	_, err := s.db.Put(m.ID, m)
	return err
}

// Cancel queues a cancellation message for each of the corresponding message IDs
func (s *QueueServer) Cancel(ctx context.Context, req *CancelRequest) (*CancelResponse, error) {
	ex, err := newExchange(s.conn, "commands")
	if err != nil {
		return nil, err
	}
	defer ex.close()

	msgs := s.getMessages(ctx, req.Ids)
	resp := CancelResponse{}
	for _, m := range msgs {
		if !(m.State == "cancelled" || m.State == "canceled") {
			continue
		}
		err := ex.cancelCommands(ctx, &m)
		if err != nil {
			return &resp, err
		}
		resp.Ids = append(resp.Ids, m.ID)
	}

	return &resp, nil
}
