// Package files defines a common interface for MLF2 data files.
package files

import (
	"encoding/json"
	"io"
	"time"

	tm "bitbucket.org/uwaplmlf2/go-mlf2/pkg/time"
)

type Metadata struct {
	Key   string
	Value interface{}
}

// CouchDbMarshaler is the interface implemented by types which can marshal
// themselves into a valid Couchdb JSON document.
type CouchDbMarshaler interface {
	MarshalCouchDb(id, rev string, meta ...Metadata) ([]byte, error)
}

// Summary document for unknown file type
type Summary struct {
	Tstart   tm.Timestamp `json:"t_start"`
	Tstop    tm.Timestamp `json:"t_stop"`
	Filetype string       `json:"filetype"`
}

func Summarize(rdr io.Reader) (*Summary, error) {
	t := time.Now().UTC()
	s := Summary{
		Tstart:   tm.Timestamp{t},
		Tstop:    tm.Timestamp{t},
		Filetype: "unknown",
	}

	return &s, nil
}

// MarshalCouchDb implements the CouchDbMarshaler interface
func (s *Summary) MarshalCouchDb(id, rev string, meta ...Metadata) ([]byte, error) {
	dict := make(map[string]interface{})
	b, err := json.Marshal(s)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(b, &dict)

	dict["_id"] = id
	if rev != "" {
		dict["_rev"] = rev
	}

	for _, m := range meta {
		dict[m.Key] = m.Value
	}

	return json.Marshal(dict)
}
