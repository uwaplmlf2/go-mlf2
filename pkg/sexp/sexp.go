// Package sexp provides functions for working with the MLF2 S-expression
// format data files.
package sexp

import (
	"encoding/json"
	"io"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	tm "bitbucket.org/uwaplmlf2/go-mlf2/pkg/time"
	sx "bitbucket.org/uwaplmlf2/mlf2sexp"
)

type Summary struct {
	Tstart   tm.Timestamp `json:"t_start"`
	Tstop    tm.Timestamp `json:"t_stop"`
	Nrecs    int          `json:"nrecs"`
	Filetype string       `json:"filetype"`
}

// Summarize returns a summary of an S-expression data file and any error
// that occurs.
func Summarize(rdr io.Reader) (*Summary, error) {
	var last_t time.Time

	scanner := sx.NewScanner(rdr)
	s := Summary{}
	for scanner.Scan() {
		rec := scanner.Record()
		last_t = rec.Time()
		s.Nrecs += 1
		if s.Nrecs == 1 {
			s.Tstart.Time = last_t
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	s.Tstop.Time = last_t
	s.Filetype = "sexp"

	return &s, nil
}

// MarshalCouchDb implements the CouchDbMarshaler interface
func (s *Summary) MarshalCouchDb(id, rev string, meta ...files.Metadata) ([]byte, error) {
	dict := make(map[string]interface{})
	b, err := json.Marshal(s)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(b, &dict)

	dict["_id"] = id
	if rev != "" {
		dict["_rev"] = rev
	}

	for _, m := range meta {
		dict[m.Key] = m.Value
	}

	return json.Marshal(dict)
}
