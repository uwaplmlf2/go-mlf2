package sexp

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func helperOpenFile(t *testing.T, name string) *os.File {
	path := filepath.Join("testdata", name)
	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	return f
}

func helperReadFile(t *testing.T, name string) []byte {
	path := filepath.Join("testdata", name)
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}
	return contents
}

func TestJSONRead(t *testing.T) {
	text := helperReadFile(t, "data0272.json")
	s := Summary{}
	if err := json.Unmarshal(text, &s); err != nil {
		t.Fatal(err)
	}

	if s.Tstart.Unix() != 1509514566 || s.Tstop.Unix() != 1509515051 {
		t.Errorf("Bad timestamps: %v, %v", s.Tstart, s.Tstop)
	}

	if s.Nrecs != 12 {
		t.Errorf("Bad record count: %d", s.Nrecs)
	}

}

func TestSummary(t *testing.T) {
	text := helperReadFile(t, "data0272.json")
	s := Summary{}
	if err := json.Unmarshal(text, &s); err != nil {
		t.Fatal(err)
	}

	f := helperOpenFile(t, "data0272.sx")
	s2, err := Summarize(f)
	if err != nil {
		t.Fatalf("%+v", err)
	}

	if *s2 != s {
		t.Errorf("Data mismatch: %+v != %+v", s2, s)
	}
}
