// Package sexp provides functions for working with the MLF2 CSV
// format data files.
package csv

import (
	"bufio"
	"encoding/json"
	"io"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	tm "bitbucket.org/uwaplmlf2/go-mlf2/pkg/time"
	"github.com/pkg/errors"
)

type Summary struct {
	Tstart   tm.Timestamp `json:"t_start"`
	Tstop    tm.Timestamp `json:"t_stop"`
	Nrecs    int          `json:"nrows"`
	Columns  []string     `json:"columns"`
	Filetype string       `json:"filetype"`
}

// Summarize returns a summary of a CSV data file and any error
// that occurs.
func Summarize(rdr io.Reader) (*Summary, error) {
	var (
		tstamp   string
		firstRec bool
	)

	scanner := bufio.NewScanner(rdr)
	s := Summary{}
	columns := make(map[string]int)
	for scanner.Scan() {
		s.Nrecs++
		if s.Nrecs == 1 {
			s.Columns = strings.Split(scanner.Text(), ",")
			for i, name := range s.Columns {
				columns[name] = i
			}
			continue
		}

		cols := strings.Split(scanner.Text(), ",")
		tstamp = cols[columns["time"]]

		if !firstRec && s.Nrecs > 1 {
			secs, err := strconv.ParseInt(tstamp, 10, 64)
			if err != nil {
				return nil, errors.Wrapf(err, "parsing time-stamp %q", tstamp)
			}
			// Hack to handle ballast files
			if secs > 1 {
				firstRec = true
			}
			s.Tstart.Time = time.Unix(secs, 0).UTC()
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	// Parse ending time-stamp
	secs, err := strconv.ParseInt(tstamp, 10, 64)
	if err != nil {
		return nil, errors.Wrapf(err, "parsing time-stamp %q", tstamp)
	}
	s.Tstop.Time = time.Unix(secs, 0).UTC()
	// Decrement the record count to skip the header line
	s.Nrecs--
	s.Filetype = "csv"

	return &s, nil
}

// MarshalCouchDb implements the CouchDbMarshaler interface
func (s *Summary) MarshalCouchDb(id, rev string, meta ...files.Metadata) ([]byte, error) {
	dict := make(map[string]interface{})
	b, err := json.Marshal(s)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(b, &dict)

	dict["_id"] = id
	if rev != "" {
		dict["_rev"] = rev
	}

	for _, m := range meta {
		dict[m.Key] = m.Value
	}

	return json.Marshal(dict)
}
