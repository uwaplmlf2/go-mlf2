// Package netcdf provides functions to work with the MLF2 netCDF
// format data files.
package netcdf

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	tm "bitbucket.org/uwaplmlf2/go-mlf2/pkg/time"
	"github.com/batchatco/go-native-netcdf/netcdf/cdf"
)

type Summary struct {
	Id         string               `json:"_id,omitempty"`
	Rev        string               `json:"_rev,omitempty"`
	Tstart     tm.Timestamp         `json:"t_start"`
	Tstop      tm.Timestamp         `json:"t_stop"`
	Dimensions map[string]int       `json:"dimensions,omitempty"`
	Variables  map[string]*Variable `json:"variables,omitempty"`
	Filetype   string               `json:"filetype"`
}

type Variable struct {
	Dimensions []string `json:"dimensions"`
	Shape      []int    `json:"shape"`
	Units      string   `json:"units,omitempty"`
	Longname   string   `json:"long_name,omitempty"`
}

type FileLike interface {
	io.Reader
	io.Writer
	Seek(int64, int) (int64, error)
}

var ErrEmpty = errors.New("Empty netCDF file")

func Summarize(f FileLike) (*Summary, error) {
	fp, err := cdf.New(f)
	if err != nil {
		return nil, err
	}
	defer fp.Close()

	v, err := fp.GetVariable("time")
	if err != nil {
		return nil, fmt.Errorf("Invalid netCDF format: %w", err)
	}
	secs, ok := v.Values.([]int32)
	if !ok {
		return nil, fmt.Errorf("Invalid timestamp format: %w", err)
	}

	if len(secs) == 0 {
		return nil, ErrEmpty
	}

	s := Summary{}
	s.Filetype = "netcdf"
	s.Variables = make(map[string]*Variable)
	s.Dimensions = make(map[string]int)

	nrecs := len(secs)
	s.Tstart.Time = time.Unix(int64(secs[0]), 0)
	s.Tstop.Time = time.Unix(int64(secs[nrecs-1]), 0)
	s.Dimensions["time"] = nrecs

	for _, name := range fp.ListVariables() {
		vr, _ := fp.GetVariable(name)
		v := Variable{}

		v.Dimensions = make([]string, len(vr.Dimensions))
		copy(v.Dimensions, vr.Dimensions)

		if av, ok := vr.Attributes.Get("units"); ok {
			v.Units = av.(string)
		}
		if av, ok := vr.Attributes.Get("long_name"); ok {
			v.Longname = av.(string)
		}
		s.Variables[name] = &v
	}

	return &s, nil
}

// MarshalCouchDb implements the CouchDbMarshaler interface
func (s *Summary) MarshalCouchDb(id, rev string, meta ...files.Metadata) ([]byte, error) {
	s.Id = id
	s.Rev = rev

	if len(meta) > 0 {
		dict := make(map[string]interface{})
		b, err := json.Marshal(s)
		if err != nil {
			return nil, err
		}
		json.Unmarshal(b, &dict)
		for _, m := range meta {
			dict[m.Key] = m.Value
		}
		return json.Marshal(dict)
	}

	return json.Marshal(s)
}
