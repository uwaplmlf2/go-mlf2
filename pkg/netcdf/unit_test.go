package netcdf

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"testing"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	"github.com/dsnet/golib/memfile"
	uuid "github.com/satori/go.uuid"
)

func helperOpenFile(t *testing.T, name string) FileLike {
	path := filepath.Join("testdata", name)
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}
	return memfile.New(contents)
}

func helperReadFile(t *testing.T, name string) []byte {
	path := filepath.Join("testdata", name)
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}
	return contents
}

func TestJSONRead(t *testing.T) {
	text := helperReadFile(t, "env0316.json")
	s := Summary{}
	if err := json.Unmarshal(text, &s); err != nil {
		t.Fatal(err)
	}

	if s.Tstart.Unix() != 1509635997 || s.Tstop.Unix() != 1509644813 {
		t.Errorf("Bad timestamps: %v, %v", s.Tstart, s.Tstop)
	}

	if len(s.Variables) != 7 {
		t.Errorf("Bad variable count: %d", len(s.Variables))
	}

	if len(s.Dimensions) != 2 {
		t.Errorf("Bad dimension count: %d", len(s.Dimensions))
	}

	if s.Dimensions["time"] != 96 {
		t.Errorf("Bad record count: %d", s.Dimensions["time"])
	}
}

func TestSummary(t *testing.T) {
	text := helperReadFile(t, "env0316.json")
	s := Summary{}
	if err := json.Unmarshal(text, &s); err != nil {
		t.Fatal(err)
	}

	f := helperOpenFile(t, "env0316.nc")
	s2, err := Summarize(f)
	if err != nil {
		t.Fatal(err)
	}

	if s2.Dimensions["time"] != 96 {
		t.Errorf("Bad record count: %d", s2.Dimensions["time"])
	}

	if !s2.Tstart.Equal(s.Tstart) {
		t.Errorf("Start time mismatch: %v != %v", s2.Tstart, s.Tstart)
	}

	if !s2.Tstop.Equal(s.Tstop) {
		t.Errorf("Stop time mismatch: %v != %v", s2.Tstop, s.Tstop)
	}

	for k, v := range s2.Variables {
		if v.Units != s.Variables[k].Units {
			t.Errorf("Variable mismatch: %+v != %+v", v, s.Variables[k])
		}
	}
}

func TestJSONWrite(t *testing.T) {
	f := helperOpenFile(t, "env0316.nc")
	s, err := Summarize(f)
	if err != nil {
		t.Fatal(err)
	}

	id := uuid.NewV4().String()
	doc, err := s.MarshalCouchDb(id, "")
	if err != nil {
		t.Fatal(err)
	}
	s2 := Summary{}
	if err := json.Unmarshal(doc, &s2); err != nil {
		t.Fatal(err)
	}

	if s.Id != s2.Id {
		t.Errorf("ID mismatch: %q != %q", s2.Id, s.Id)
	}
}

func TestJSONWriteMeta(t *testing.T) {
	f := helperOpenFile(t, "env0316.nc")
	s, err := Summarize(f)
	if err != nil {
		t.Fatal(err)
	}

	meta := []files.Metadata{
		{Key: "type", Value: "data"},
		{Key: "foo", Value: "bar"},
	}

	id := uuid.NewV4().String()
	doc, err := s.MarshalCouchDb(id, "", meta...)
	if err != nil {
		t.Fatal(err)
	}
	dict := make(map[string]interface{})
	if err := json.Unmarshal(doc, &dict); err != nil {
		t.Fatal(err)
	}

	if dict["type"].(string) != "data" {
		t.Errorf("Field mismatch: %v != \"data\"", dict["type"])
	}
}
