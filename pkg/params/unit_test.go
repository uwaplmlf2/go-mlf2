package params

import (
	"encoding/xml"
	"testing"
	"time"
)

var t_expect = time.Date(2015, time.March, 9, 18, 4, 22, 0, time.UTC)
var CONTENT = `<ptable time='1425924262'>
<param name='sexp:subsample' type='long'>16</param>
<param name='comm:checkreg' type='short'>1</param>
<param name='slow.bocha3.BspeedEtime' type='double'>1e+07</param>
<param name='slow.bocha3.Bspeed' type='double'>1e-07</param>
<param name='ballast.S0' type='double'>37.45</param>
<param name='drift.iso_Gamma' type='double'>1</param>
<param name='surface:gps_fixes' type='short'>10</param>
</ptable>
`

var UPDATE = `<ptable time='1425924272'>
<param name='ballast.S0' type='double'>38.45</param>
</ptable>
`

var MULTI = "<tables>" + CONTENT + UPDATE + "</tables>"

func TestPtDecode(t *testing.T) {
	p := Ptable{}
	err := xml.Unmarshal([]byte(CONTENT), &p)
	if err != nil {
		t.Fatal(err)
	}

	if !t_expect.Equal(p.T) {
		t.Errorf("Wrong timestamp; expected %q, got %q", t_expect, p.T)
	}

	if v := p.Lookup("ballast.s0"); v.Value != "37.45" {
		t.Errorf("Parameter mismatch; expected 37.45, got %q", v.Value)
	}

	err = xml.Unmarshal([]byte(UPDATE), &p)
	if err != nil {
		t.Fatal(err)
	}

	if p.Len() != 7 {
		t.Errorf("Wrong element count after update: %d", p.Len())
	}

	if v := p.Lookup("Ballast.S0"); v.Value != "38.45" {
		t.Errorf("Parameter mismatch; expected 38.45, got %q", v.Value)
	}
}

func TestPtListDecode(t *testing.T) {
	p := PtableList{}
	err := xml.Unmarshal([]byte(MULTI), &p)
	if err != nil {
		t.Fatal(err)
	}

	if p.Len() != 7 {
		t.Errorf("Wrong element count; expected 7, got %d", p.Len())
	}

	if v := p.Lookup("Ballast.S0"); v.Value != "38.45" {
		t.Errorf("Parameter mismatch; expected 38.45, got %q", v.Value)
	}
}

func TestEncode(t *testing.T) {
	mp := MissionParameters{}
	mp.Length = time.Hour * 24
	mp.Params = map[string]Parameter{
		"start_depth": {Name: "start_depth", Value: "1"},
		"countdown":   {Name: "countdown", Value: "10"},
	}

	out, err := xml.Marshal(mp)
	if err != nil {
		t.Fatal(err)
	}

	mp2 := MissionParameters{}
	err = xml.Unmarshal(out, &mp2)
	if err != nil {
		t.Fatal(err)
	}

	if mp.Length != mp2.Length {
		t.Errorf("Duration mismatch; expected %v, got %v", mp.Length, mp2.Length)
	}

	for k, v := range mp.Params {
		if mp2.Params[k].Value != v.Value {
			t.Errorf("Parameter mismatch; expected %v, got %v", v, mp2.Params[k])
		}
	}
}
