// Package params processes MLF2 XML parameter files.
package params

import (
	"encoding/xml"
	"strconv"
	"strings"
	"time"
)

// Parameter contains a single mission parameter setting
type Parameter struct {
	XMLName xml.Name `xml:"param"`
	Name    string   `xml:"name,attr"`
	Value   string   `xml:"value,attr"`
}

type PtElement struct {
	XMLName xml.Name `xml:"param"`
	Name    string   `xml:"name,attr"`
	Type    string   `xml:"type,attr"`
	Value   string   `xml:",chardata"`
}

type Duration time.Duration
type Timestamp time.Time

// MissionDuration encodes the length of the deployment
type MissionDuration struct {
	XMLName xml.Name `xml:"duration"`
	Units   string   `xml:"units,attr"`
	Value   Duration `xml:"value,attr"`
}

type missionFile struct {
	XMLName xml.Name        `xml:"mission"`
	Length  MissionDuration `xml:"duration"`
	Params  []Parameter     `xml:"param"`
}

type ptFile struct {
	XMLName xml.Name    `xml:"ptable"`
	T       Timestamp   `xml:"time,attr"`
	Params  []PtElement `xml:"param"`
}

type ptListFile struct {
	XMLName xml.Name `xml:"tables"`
	Tables  []ptFile `xml:"ptable"`
}

// A set of mission parameters and duration
type MissionParameters struct {
	Length time.Duration
	Params map[string]Parameter
}

// A set of mission parameters and a timestamp
type Ptable struct {
	T      time.Time
	params map[string]PtElement
}

type PtableList struct {
	Ptable
}

func (d Duration) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	secs := int64(time.Duration(d) / time.Second)

	attr := xml.Attr{
		Name:  name,
		Value: strconv.FormatInt(secs, 10),
	}
	return attr, nil
}

func (d *Duration) UnmarshalXMLAttr(attr xml.Attr) error {
	secs, err := strconv.ParseInt(attr.Value, 10, 64)
	if err != nil {
		return err
	}
	*d = Duration(time.Duration(secs) * time.Second)
	return nil
}

func (t Timestamp) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	attr := xml.Attr{
		Name:  name,
		Value: strconv.FormatInt(time.Time(t).Unix(), 10),
	}
	return attr, nil
}

func (t *Timestamp) UnmarshalXMLAttr(attr xml.Attr) error {
	secs, err := strconv.ParseInt(attr.Value, 10, 64)
	if err != nil {
		return err
	}
	*t = Timestamp(time.Unix(secs, 0).UTC())
	return nil
}

func (mp *MissionParameters) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var proxy missionFile
	err := d.DecodeElement(&proxy, &start)
	if err != nil {
		return err
	}

	mp.Length = time.Duration(proxy.Length.Value)
	mp.Params = make(map[string]Parameter)
	for _, p := range proxy.Params {
		mp.Params[p.Name] = p
	}

	return nil
}

func (mp MissionParameters) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	var proxy missionFile
	proxy.Length = MissionDuration{
		Units: "seconds",
		Value: Duration(mp.Length),
	}
	proxy.Params = make([]Parameter, 0, len(mp.Params))
	for _, v := range mp.Params {
		proxy.Params = append(proxy.Params, v)
	}

	start.Name.Local = "mission"
	return e.EncodeElement(proxy, start)
}

func (p *Ptable) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var proxy ptFile
	err := d.DecodeElement(&proxy, &start)
	if err != nil {
		return err
	}

	p.T = time.Time(proxy.T)
	if p.params == nil {
		p.params = make(map[string]PtElement)
	}

	for _, e := range proxy.Params {
		p.params[strings.ToLower(e.Name)] = e
	}

	return nil
}

func (p *Ptable) Len() int {
	return len(p.params)
}

func (p *Ptable) Lookup(key string) PtElement {
	return p.params[strings.ToLower(key)]
}

func (p *Ptable) Update(key string, val PtElement) {
	p.params[strings.ToLower(key)] = val
}

func (p *PtableList) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var proxy ptListFile
	err := d.DecodeElement(&proxy, &start)
	if err != nil {
		return err
	}

	p.Ptable.T = time.Time(proxy.Tables[len(proxy.Tables)-1].T)
	if p.Ptable.params == nil {
		p.Ptable.params = make(map[string]PtElement)
	}

	for _, table := range proxy.Tables {
		for _, e := range table.Params {
			p.Ptable.params[strings.ToLower(e.Name)] = e
		}
	}

	return nil
}
