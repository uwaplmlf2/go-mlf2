package syslog

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func helperOpenFile(t *testing.T, name string) *os.File {
	path := filepath.Join("testdata", name)
	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	return f
}

func helperReadFile(t *testing.T, name string) []byte {
	path := filepath.Join("testdata", name)
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}
	return contents
}

func TestSummary(t *testing.T) {
	f := helperOpenFile(t, "syslog0.txt")
	s, err := Summarize(f)
	if err != nil {
		t.Fatal(err)
	}

	text := helperReadFile(t, "syslog0.json")
	s2 := Summary{}
	if err := json.Unmarshal(text, &s2); err != nil {
		t.Fatal(err)
	}

	if s.Nrecs != s2.Nrecs {
		t.Errorf("Bad record count: %d != %d", s.Nrecs, s2.Nrecs)
	}

	if !s.Tstart.Equal(s2.Tstart) {
		t.Errorf("Start time mismatch: %v != %v", s.Tstart.Time, s2.Tstart.Time)
	}

	if !s.Tstop.Equal(s2.Tstop) {
		t.Errorf("Start time mismatch: %v != %v", s.Tstop.Time, s2.Tstop.Time)
	}

}
