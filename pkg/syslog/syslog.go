// Package syslog provides functions for working with MLF2 system
// log files.
package syslog

import (
	"bufio"
	"encoding/json"
	"io"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	tm "bitbucket.org/uwaplmlf2/go-mlf2/pkg/time"
)

const TimeFormat = "2006-01-02 15:04:05"

type Summary struct {
	Tstart   tm.Timestamp `json:"t_start"`
	Tstop    tm.Timestamp `json:"t_stop"`
	Nrecs    int          `json:"nrecs"`
	Filetype string       `json:"filetype"`
}

// Summarize returns a summary of a CSV data file and any error
// that occurs.
func Summarize(rdr io.Reader) (*Summary, error) {
	var tstamp string

	scanner := bufio.NewScanner(rdr)
	s := Summary{}
	for scanner.Scan() {
		s.Nrecs++
		tstamp = string(scanner.Bytes()[0:19])
		if s.Nrecs == 1 {
			t, _ := time.Parse(TimeFormat, tstamp)
			s.Tstart = tm.Timestamp{t.UTC()}
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	t, _ := time.Parse(TimeFormat, tstamp)
	s.Tstop = tm.Timestamp{t.UTC()}
	s.Filetype = "syslog"

	return &s, nil
}

// MarshalCouchDb implements the CouchDbMarshaler interface
func (s *Summary) MarshalCouchDb(id, rev string, meta ...files.Metadata) ([]byte, error) {
	dict := make(map[string]interface{})
	b, err := json.Marshal(s)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(b, &dict)

	dict["_id"] = id
	if rev != "" {
		dict["_rev"] = rev
	}

	for _, m := range meta {
		dict[m.Key] = m.Value
	}

	return json.Marshal(dict)
}
