// Package messaging handles MLF2 command messages and responses. The messages
// and responses flow between a CouchDB database, AMQP message broker, and the
// floats.
package messages

import (
	"encoding/json"
	"strings"
	"time"
)

// Time is a timestamp for messages.
type Time struct {
	time.Time
}

// UnmarshalJSON implements the json.Unmarshal interface, it unmarshals a new
// Time value from integer seconds since the Unix Epoch.
func (mt *Time) UnmarshalJSON(b []byte) error {
	var tsecs int64
	if err := json.Unmarshal(b, &tsecs); err != nil {
		return err
	}
	if tsecs == 0 {
		*mt = Time{}
	} else {
		mt.Time = time.Unix(tsecs, 0).UTC()
	}
	return nil
}

// MarshalJSON implements the json.Marshal interface, it marshals a Time value
// to integer seconds since the Unix Epoch.
func (mt Time) MarshalJSON() ([]byte, error) {
	var tsecs int64
	if mt.Time.IsZero() {
		tsecs = 0
	} else {
		tsecs = mt.Time.Unix()
	}
	return json.Marshal(tsecs)
}

// Command is a command to be sent to an MLF2 float
type Command struct {
	Name string
	Args []string
}

// String implements the Stringer interface
func (c Command) String() string {
	return c.Name + " " + strings.Join(c.Args, " ")
}

// CommandReply contains the command sent and the response received
// from an MLF2 float
type CommandReply struct {
	Command  string `json:"q"`
	Response string `json:"a"`
}

// Response contains all of the CommandReplys from a Message
type Response struct {
	ID       string         `json:"_id"`
	Rev      string         `json:"_rev,omitempty"`
	Floatid  int            `json:"floatid"`
	Ref      string         `json:"ref,omitempty"`
	Type     string         `json:"type"`
	Notify   string         `json:"notify"`
	Received Time           `json:"t_received,omitempty"`
	Contents []CommandReply `json:"contents"`
}

// String implements the Stringer interface
func (r *Response) String() string {
	lines := make([]string, 0)
	for _, reply := range r.Contents {
		lines = append(lines, "> "+reply.Command)
		lines = append(lines, reply.Response)
	}
	return strings.Join(lines, "\n")
}

// Message contains one or more commands which are scheduled to be sent
// to an MLF2 float on its next surfacing.
type Message struct {
	ID        string          `json:"_id"`
	Rev       string          `json:"_rev,omitempty"`
	Floatid   int             `json:"floatid"`
	Comment   string          `json:"comment"`
	Sender    string          `json:"sender"`
	State     string          `json:"state"`
	Sticky    bool            `json:"sticky"`
	Ref       string          `json:"ref,omitempty"`
	Type      string          `json:"type"`
	Submitted Time            `json:"t_submitted,omitempty"`
	Delivered Time            `json:"t_delivered,omitempty"`
	Queued    Time            `json:"t_queued,omitempty"`
	Contents  [][]interface{} `json:"contents"`
}

// Count returns the number of commands in the message
func (m *Message) Count() int {
	return len(m.Contents)
}

// Commands returns the contents of the Message as a slice of Commands
func (m *Message) Commands() []Command {
	var (
		ok   bool
		args []interface{}
	)

	cmds := make([]Command, 0, len(m.Contents))
	for _, e := range m.Contents {
		cmd := Command{}
		cmd.Name, ok = e[0].(string)
		args, ok = e[1].([]interface{})
		if ok {
			cmd.Args = make([]string, 0, len(args))
			for _, a := range args {
				cmd.Args = append(cmd.Args, a.(string))
			}
		}
		cmds = append(cmds, cmd)
	}

	return cmds
}

// AddCommand adds a new Command to the Message
func (m *Message) AddCommand(cmd Command) {
	entry := make([]interface{}, 2)
	entry[0] = cmd.Name
	sub := make([]interface{}, 0, len(cmd.Args))
	for _, a := range cmd.Args {
		sub = append(sub, a)
	}
	entry[1] = sub
	m.Contents = append(m.Contents, entry)
}

// Clone creates a new Message from an existing one
func (m *Message) Clone() *Message {
	m2 := Message{
		Floatid: m.Floatid,
		Sender:  m.Sender,
		Comment: m.Comment,
		Type:    "message",
		Sticky:  m.Sticky,
		State:   "ready",
	}
	for _, c := range m.Commands() {
		m2.AddCommand(c)
	}

	return &m2
}
