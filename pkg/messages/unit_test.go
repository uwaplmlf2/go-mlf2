package messages

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"testing"
)

var RESPONSE = `
{
   "_id": "c0d6f7ed5d1137df8d925bc02cc438de",
   "_rev": "2-f9587111cd395043db418118ff11e6c8",
   "t_received": 0,
   "type": "response",
   "floatid": 74,
   "notify": "ashcherbina@apl.washington.edu",
   "ref": "c0d6f7ed5d1137df8d925bc02cc416bd",
   "contents": [
       {
           "q": "wait",
           "a": "Waiting on the surface"
       }
   ]
}
`

func helperReadFile(t *testing.T, name string) []byte {
	path := filepath.Join("testdata", name)
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}
	return contents
}

func TestReadJson(t *testing.T) {
	text := helperReadFile(t, "sample.json")
	m := Message{}
	if err := json.Unmarshal(text, &m); err != nil {
		t.Fatal(err)
	}

	if m.Count() != 2 {
		t.Errorf("Wrong content length: %d", m.Count())
	}

	cmds := m.Commands()
	if cmds[0].Name != "send-file" {
		t.Errorf("Wrong command: %q", cmds[0].Name)
	}

	if len(cmds[0].Args) != 1 {
		t.Errorf("Wrong argument list length: %d", len(cmds[0].Args))
	}

	if cmds[0].String() != "send-file syslog0.txt" {
		t.Errorf("Wrong command argument: %q", cmds[0].Args[0])
	}
}

func TestAddCommands(t *testing.T) {
	m := Message{}
	commands := []Command{
		{Name: "send-file", Args: []string{"syslog0.txt"}},
		{Name: "set", Args: []string{"comm:wait_timeout", "3600"}},
		{Name: "get", Args: []string{"min_motor_battery"}},
		{Name: "set", Args: []string{"sleep_duration", "20000"}},
	}

	for _, cmd := range commands {
		m.AddCommand(cmd)
	}

	if m.Count() != len(commands) {
		t.Errorf("Wrong command count: %d != %d", m.Count(), len(commands))
	}

	cmds := m.Commands()
	for i, cmd := range cmds {
		if cmd.String() != commands[i].String() {
			t.Errorf("Command mismatch: %v != %v", cmd, commands[i])
		}
	}
}

func TestCloneMessage(t *testing.T) {
	text := helperReadFile(t, "sample.json")
	m := Message{}
	if err := json.Unmarshal(text, &m); err != nil {
		t.Fatal(err)
	}

	m2 := m.Clone()

	if m2.State != "ready" {
		t.Errorf("Wrong state for cloned Message: %v", m2.State)
	}

	if m2.Floatid != m.Floatid {
		t.Errorf("Expected floatid %d; got %d", m.Floatid, m2.Floatid)
	}

	if m2.Sender != m.Sender {
		t.Errorf("Expected sender %v; got %v", m.Sender, m2.Sender)
	}

	if m2.Comment != m.Comment {
		t.Errorf("Expected comment %v; got %v", m.Comment, m2.Comment)
	}

	cmds := m.Commands()
	cmds2 := m2.Commands()
	for i, cmd := range cmds {
		if cmd.String() != cmds2[i].String() {
			t.Errorf("Command mismatch: %v != %v", cmd, cmds2[i])
		}
	}
}

func TestTimeJson(t *testing.T) {
	tm := Time{}
	b, err := json.Marshal(tm)
	if err != nil {
		t.Fatal(err)
	}
	if string(b) != "0" {
		t.Errorf("Expected 0; got %s", b)
	}

	tm2 := Time{}
	err = json.Unmarshal(b, &tm2)
	if err != nil {
		t.Fatal(err)
	}
	if !tm2.Time.IsZero() {
		t.Errorf("Expected 0 Time value; got %v", tm2)
	}
}

func TestResponseStringify(t *testing.T) {
	resp := Response{}
	err := json.Unmarshal([]byte(RESPONSE), &resp)
	if err != nil {
		t.Fatal(err)
	}

	expected := "> wait\nWaiting on the surface"
	if resp.String() != expected {
		t.Errorf("Expected %q; got %q", expected, resp.String())
	}
}
