package status

import (
	"encoding/json"
	"encoding/xml"
	"reflect"
	"testing"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
)

var XMLDATA = `
<status>
<date>2017-08-08 01:33:42</date>
<gps nsats='5' fmt='deg'>47.655125/-122.317299</gps>
<irsq>0</irsq>
<df>124626944</df>
<rh>36.0</rh>
<ipr>2.5</ipr>
<v>864/449</v>
<ep>0</ep>
<alert>This is a test alert</alert>
<alert>Questionable GPS fix</alert>
</status>
`

var expected Status = Status{
	Timestamp:   "2017-08-08T01:33:42+00:00",
	Mode:        "mission",
	Gps:         GpsData{Nsat: 5, Lat: 47.655125, Lon: -122.317299},
	Sensors:     SensorData{Ipr: 2.5, Humidity: 36, Voltage: []int{864, 449}},
	Irsq:        0,
	PistonError: 0,
	Freespace:   124626944,
	Alerts:      []string{"This is a test alert", "Questionable GPS fix"},
}

func TestXMLParse(t *testing.T) {
	var s Status
	err := xml.Unmarshal([]byte(XMLDATA), &s)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(s, expected) {
		t.Errorf("Bad value; expected %#v, got %#v", expected, s)
	}

	b, err := json.Marshal(&s)
	if err != nil {
		t.Error(err)
	}
	t.Logf("%q", b)
}

func TestCouchDBFormat(t *testing.T) {
	var s Status
	err := xml.Unmarshal([]byte(XMLDATA), &s)
	if err != nil {
		t.Fatal(err)
	}

	md := []files.Metadata{
		{Key: "floatid", Value: 42},
		{Key: "type", Value: "data"},
		{Key: "t_recv", Value: time.Now().Unix()},
	}

	id := "somerandomID"
	rev := "1-6fb6eb8c242594ad9ac9ed16320f29ef"
	b, err := s.MarshalCouchDb(id, rev, md...)
	if err != nil {
		t.Fatal(err)
	}

	s2 := Status{}
	if err = json.Unmarshal(b, &s2); err != nil {
		t.Fatal(err)
	}

	if s2.ID != id {
		t.Errorf("Wrong ID: got %q; expected %q", s2.ID, id)
	}

	if s2.Rev != rev {
		t.Errorf("Wrong Rev: got %q; expected %q", s2.Rev, rev)
	}

	if s2.Floatid != 42 {
		t.Errorf("Wrong Floatid: got %d; expected 42", s2.Floatid)
	}

}

func TestDmm(t *testing.T) {
	var table = []struct {
		in  float64
		out string
	}{
		{in: 30.5, out: "30-30.0000"},
		{in: -0.5, out: "-0-30.0000"},
		{in: 47.25, out: "47-15.0000"},
	}

	for _, e := range table {
		d := NewDmm(e.in)
		if d.String() != e.out {
			t.Errorf("Output mismatch: %q != %q", d.String(), e.out)
		}
	}
}
