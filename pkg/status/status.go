// Package status contains functions used to process MLF2 status messages.
package status

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"math"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
)

const TimeFormat = "2006-01-02T15:04:05-07:00"

type mlf2Time struct {
	time.Time
}

type gpsInfo struct {
	Nsat     int    `xml:"nsats,attr"`
	Position string `xml:",chardata"`
}

type statusXml struct {
	XMLName     xml.Name
	Date        mlf2Time `xml:"date"`
	Gps         gpsInfo  `xml:"gps"`
	Irsq        int      `xml:"irsq"`
	Freespace   int      `xml:"df"`
	Humidity    float32  `xml:"rh"`
	Ipr         float32  `xml:"ipr"`
	Voltage     string   `xml:"v"`
	PistonError int      `xml:"ep"`
	Alerts      []string `xml:"alert"`
}

type GpsData struct {
	Nsat int     `json:"nsat"`
	Lat  float64 `json:"lat"`
	Lon  float64 `json:"lon"`
}

type SensorData struct {
	Ipr      float32 `json:"ipr"`
	Humidity float32 `json:"rh"`
	Voltage  []int   `json:"voltage"`
}

type Status struct {
	ID          string     `json:"_id,omitempty"`
	Rev         string     `json:"_rev,omitempty"`
	Type        string     `json:"type"`
	Received    int64      `json:"t_recv"`
	Floatid     int        `json:"floatid"`
	Timestamp   string     `json:"timestamp"`
	Mode        string     `json:"mode"`
	Gps         GpsData    `json:"gps"`
	Sensors     SensorData `json:"sensors"`
	Irsq        int        `json:"irsq"`
	PistonError int        `json:"piston_error"`
	Freespace   int        `json:"freespace"`
	Alerts      []string   `json:"alerts,omitempty"`
}

func (m *mlf2Time) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	const format = "2006-01-02 15:04:05"
	var v string
	d.DecodeElement(&v, &start)
	tval, err := time.Parse(format, v)
	if err != nil {
		return err
	}
	*m = mlf2Time{tval.UTC()}
	return nil
}

func (s *Status) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var proxy statusXml
	err := d.DecodeElement(&proxy, &start)
	if err != nil {
		return err
	}

	t := proxy.Date.Time.Truncate(time.Second)
	s.Timestamp = t.Format(TimeFormat)
	s.Mode = proxy.XMLName.Local
	if s.Mode == "status" {
		s.Mode = "mission"
	}
	s.Gps.Nsat = proxy.Gps.Nsat
	s.Sensors.Ipr = proxy.Ipr
	s.Sensors.Humidity = proxy.Humidity
	s.Irsq = proxy.Irsq
	s.PistonError = proxy.PistonError
	s.Freespace = proxy.Freespace
	s.Alerts = proxy.Alerts

	_, err = fmt.Sscanf(proxy.Gps.Position, "%f/%f", &s.Gps.Lat, &s.Gps.Lon)
	if err != nil {
		return err
	}

	s.Sensors.Voltage = make([]int, 2)
	_, err = fmt.Sscanf(proxy.Voltage, "%d/%d", &s.Sensors.Voltage[0],
		&s.Sensors.Voltage[1])

	return err
}

// MarshalCouchDb implements the CouchDbMarshaler interface
func (s *Status) MarshalCouchDb(id, rev string, meta ...files.Metadata) ([]byte, error) {
	s.ID, s.Rev = id, rev
	s.Type = "status"
	for _, m := range meta {
		switch m.Key {
		case "t_recv":
			switch v := m.Value.(type) {
			case int:
				s.Received = int64(v)
			case int64:
				s.Received = v
			case int32:
				s.Received = int64(v)
			}
		case "floatid":
			switch v := m.Value.(type) {
			case int:
				s.Floatid = v
			case int64:
				s.Floatid = int(v)
			case int32:
				s.Floatid = int(v)
			}
		}
	}

	return json.Marshal(s)
}

// Degrees and decimal minutes
type Dmm struct {
	degrees, minutes float64
}

// NewDmm creates a new Dmm instance from a decimal degree value
func NewDmm(deg float64) Dmm {
	d := Dmm{}
	if deg < 0 {
		d.degrees = math.Ceil(deg)
	} else if deg > 0 {
		d.degrees = math.Floor(deg)
	} else {
		d.degrees = deg
	}
	d.minutes = (deg - float64(d.degrees)) * 60

	return d
}

// String implements the Stringer interface
func (d Dmm) String() string {
	if d.minutes < 0 {
		d.minutes = -d.minutes
	}
	return fmt.Sprintf("%.0f-%07.4f", d.degrees, d.minutes)
}

// IsNeg returns true if the value is less than zero
func (d Dmm) IsNeg() bool {
	return (d.degrees < 0 || d.minutes < 0)
}

// Degrees converts the Dmm to decimal degrees
func (d Dmm) Degrees() float64 {
	return d.degrees + d.minutes/60.
}

// Abs returns the absolute value of d
func (d Dmm) Abs() Dmm {
	dabs := Dmm{}
	dabs.degrees = math.Abs(d.degrees)
	dabs.minutes = math.Abs(d.minutes)
	return dabs
}
