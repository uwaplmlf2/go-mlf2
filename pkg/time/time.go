// Package time extends time.Time to serialize the value as integer
// seconds since 1/1/1970 UTC.
package time

import (
	"encoding/json"
	"strconv"
	"time"
)

type Timestamp struct {
	time.Time
}

func (ts Timestamp) MarshalJSON() ([]byte, error) {
	return json.Marshal(ts.Unix())
}

func (ts *Timestamp) UnmarshalJSON(b []byte) error {
	var secs int64
	if err := json.Unmarshal(b, &secs); err != nil {
		return err
	}
	ts.Time = time.Unix(secs, 0).UTC()
	return nil
}

func (ts Timestamp) Equal(ts1 Timestamp) bool {
	return ts.Time.Equal(ts1.Time)
}

// MarshalText implements the encoding.TextMarshaler interface.
func (ts Timestamp) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(ts.Time.Unix(), 10)), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (ts *Timestamp) UnmarshalText(data []byte) error {
	secs, err := strconv.ParseInt(string(data), 10, 64)
	if err == nil {
		ts.Time = time.Unix(secs, 0).UTC()
	}

	return err
}
