package time

import (
	"testing"
	"time"
)

const FORMAT = "2006-01-02T15:04:05-07:00"

var table = []struct {
	text string
	secs string
}{
	{text: "1992-10-24T21:00:00-07:00", secs: "719985600"},
	{text: "1997-01-29T17:00:00-08:00", secs: "854586000"},
}

func TestSerialize(t *testing.T) {
	var (
		ts  Timestamp
		err error
	)

	for _, entry := range table {
		ts.Time, err = time.Parse(FORMAT, entry.text)
		if err != nil {
			t.Fatalf("Parsing %q: %v", entry.text, err)
		}
		out, err := ts.MarshalText()
		if err != nil {
			t.Error(err)
		}
		if string(out) != entry.secs {
			t.Errorf("Bad encoding %q != %q", out, entry.secs)
		}
	}
}
