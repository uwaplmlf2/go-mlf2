// Package mq contains functions for managing the MLF2 message queues.
package mq

import (
	"log"
	"sync"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

type Exchange struct {
	conn        *amqp.Connection
	channel     *amqp.Channel
	name, etype string
}

type Consumer struct {
	*Exchange
	tags []string
	wg   sync.WaitGroup
}

type Producer struct {
	*Exchange
}

type DeliveryHandler func(<-chan amqp.Delivery)

// NewExchange initializes the connection to an AMQP server and creates an exchange
func NewExchange(amqpURI, exchange, exchangeType string) (*Exchange, error) {
	ex := &Exchange{}

	var err error

	log.Println("Connecting to AMQP server")
	ex.conn, err = amqp.Dial(amqpURI)
	if err != nil {
		return nil, errors.Wrap(err, "dial")
	}

	log.Printf("got Connection, getting Channel")
	ex.channel, err = ex.conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "open channel")
	}

	ex.name, ex.etype = exchange, exchangeType

	if ex.name != "" {
		log.Printf("got Channel, declaring Exchange (%s)", exchange)
		if err = ex.channel.ExchangeDeclare(
			ex.name,  // name of the exchange
			ex.etype, // type
			true,     // durable
			false,    // delete when complete
			false,    // internal
			false,    // noWait
			nil,      // arguments
		); err != nil {
			return nil, errors.Wrap(err, "exchange declare")
		}
	}

	return ex, nil
}

// Reopen opens a new channel on the AMQP connection. This should be done in
// the event of a communication error.
func (e *Exchange) Reopen() error {
	var err error

	e.channel, err = e.conn.Channel()
	if err != nil {
		return errors.Wrap(err, "open channel")
	}

	if err = e.channel.ExchangeDeclare(
		e.name,  // name of the exchange
		e.etype, // type
		true,    // durable
		false,   // delete when complete
		false,   // internal
		false,   // noWait
		nil,     // arguments
	); err != nil {
		return errors.Wrap(err, "exchange declare")
	}

	return nil
}

// Shutdown closes the connection to the AMQP server
func (e *Exchange) Shutdown() error {
	if err := e.conn.Close(); err != nil {
		return errors.Wrap(err, "close")
	}
	return nil
}

// Publish sends a message to a named queue on the exchange.
func (e *Exchange) Publish(queue string, msg amqp.Publishing) error {
	state, err := e.channel.QueueDeclare(
		queue, // our queue name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)

	if err != nil {
		return errors.Wrap(err, "queue declare")
	}

	return e.channel.Publish(
		e.name,
		state.Name,
		false, // mandatory
		false, // immediate
		msg)
}

// CloseNotifier returns a channel to notify the caller when the connection closes.
func (e *Exchange) CloseNotifier() chan *amqp.Error {
	return e.channel.NotifyClose(make(chan *amqp.Error))
}

// NewConsumer creates a new Consumer struct and initializes the connection to the server.
func NewConsumer(amqpURI, exchange, exchangeType string) (*Consumer, error) {
	var err error

	c := &Consumer{}
	c.Exchange, err = NewExchange(amqpURI, exchange, exchangeType)
	if err != nil {
		return nil, err
	}
	c.tags = make([]string, 0)

	return c, nil
}

// Start starts a message delivery handling function in a goroutine
func (c *Consumer) Start(tag, qname, qkey string, dh DeliveryHandler) error {

	var (
		err   error
		state amqp.Queue
	)

	channel := c.Exchange.channel
	if qname == "" {
		state, err = channel.QueueDeclare(
			qname, // name of the queue
			false, // durable
			true,  // delete when usused
			true,  // exclusive
			false, // noWait
			nil,   // arguments
		)
	} else {
		state, err = channel.QueueDeclare(
			qname, // name of the queue
			true,  // durable
			false, // delete when usused
			false, // exclusive
			false, // noWait
			nil,   // arguments
		)
	}

	if err != nil {
		return errors.Wrap(err, "queue declare")
	}

	if err = channel.QueueBind(
		state.Name,      // name of the queue
		qkey,            // bindingKey
		c.Exchange.name, // sourceExchange
		false,           // noWait
		nil,             // arguments
	); err != nil {
		return errors.Wrap(err, "queue bind")
	}

	deliveries, err := channel.Consume(
		state.Name, // name
		tag,        // consumerTag,
		false,      // noAck
		false,      // exclusive
		false,      // noLocal
		false,      // noWait
		nil,        // arguments
	)
	if err != nil {
		return errors.Wrap(err, "queue consume")
	}

	c.tags = append(c.tags, tag)
	c.wg.Add(1)
	go func() {
		defer c.wg.Done()
		dh(deliveries)
	}()

	return nil
}

// Wait blocks until all message delivery goroutines have finished
func (c *Consumer) Wait() {
	log.Println("Waiting for message handlers to exit ...")
	c.wg.Wait()
}

// Shutdown stops all message delivery goroutines and closes the connection to
// the server.
func (c *Consumer) Shutdown() error {
	// will close() the deliveries channels
	for _, tag := range c.tags {
		log.Printf("Cancel %q", tag)
		if err := c.Exchange.channel.Cancel(tag, false); err != nil {
			return errors.Wrap(err, "cancel")
		}
	}

	if err := c.Exchange.conn.Close(); err != nil {
		return errors.Wrap(err, "close")
	}

	defer log.Printf("AMQP shutdown OK")

	return nil
}

// MessageLogger is an example message delivery function which can be passed to
// the Start method.
func MessageLogger(deliveries <-chan amqp.Delivery) {
	for d := range deliveries {
		log.Printf(
			"got %dB delivery: [%v] %s",
			len(d.Body),
			d.DeliveryTag,
			d.Body,
		)
		d.Ack(true)
	}
	log.Printf("deliveries channel closed")
}

// NewProducer creates a new Producer struct and initializes the connection to the server.
func NewProducer(amqpURI, exchange, exchangeType string) (*Producer, error) {
	p := &Producer{}
	var err error
	p.Exchange, err = NewExchange(amqpURI, exchange, exchangeType)
	return p, err
}

func (p *Producer) Channel() *amqp.Channel {
	return p.channel
}
