## Service Configuration Files

This directory contains templates for the service configuration files. Log-in credentials must be provided before the files can be created. Create a file in this directory named `.vars` with the following Bash-compatible variable assignments:

``` shell
MQUSER= # RabbitMq user name
MQPASS= # RabbitMq password
DBUSER= # CouchDB user name
DBPASS= # CouchDB password
SLACKHOOK= # Slack hook endpoint for status messages
```
