#!/usr/bin/env bash

source ./.vars || {
    echo "Missing variables file" 1>&2
    exit 1
}

sed -e "s!@MQUSER@!$MQUSER!g" \
    -e "s!@MQPASS@!$MQPASS!g" \
    -e "s!@DBUSER@!$DBUSER!g" \
    -e "s!@DBPASS@!$DBPASS!g" \
    -e "s!@SLACKHOOK@!$SLACKHOOK!g" \
    "$1" > "$2"
