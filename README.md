# MLF2 Data Management Software

This repository contains the software manages the incoming MLF2 data and the outgoing command messages. The software is organized as a set of Systemd services encapsulated as [portable services](https://systemd.io/PORTABLE_SERVICES/).

## Installation

The software can be installed on a 32-bit of 64-bit Linux system. Download the appropriate tar-ball from the Downloads section of this repository and unpack it on the target system. The target system must have the following software installed.

- Systemd v239 or later
- CouchDB v2+
- RabbitMq messaging server

It is possible to run the CouchDB and RabbitMq servers on separate hosts but the configuration file templates in `./config` will need to be edited accordingly.

The first step is add the database and messaging server log-in credentials to the service configuration files, see the README file in the `./config` subdirectory for instructions.

If there is a previous version of the software already running, the services must be stopped and the existing image "detached". This can be done using the `svcstop.sh` script:

``` shellsession
$ sudo ./scripts/svcstop.sh
```

Build a new image, attach it and start the services:

``` shellsession
$ make all
$ sudo ./scripts/svcstart.sh
```
