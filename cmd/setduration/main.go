// Set the duration value in an MLF2 mission XML file
package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/params"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: setduration [options] duration [file]

Set the duration value in a mission XML file. If no filename is given
on the command line, standard input is read. The new file is written
to standard output.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	d, err := time.ParseDuration(args[0])
	if err != nil {
		log.Fatalf("Parse duration %q: %v", args[0], err)
	}

	var dec *xml.Decoder
	if len(args) == 2 {
		f, err := os.Open(args[1])
		if err != nil {
			log.Fatalf("open %q: %v", args[1], err)
		}
		dec = xml.NewDecoder(f)
	} else {
		dec = xml.NewDecoder(os.Stdin)
	}

	mp := params.MissionParameters{}
	err = dec.Decode(&mp)
	if err != nil {
		log.Fatalf("XML decode: %v", err)
	}
	mp.Length = d
	delete(mp.Params, "duration")

	output, err := xml.MarshalIndent(mp, "  ", "    ")
	if err != nil {
		log.Fatalf("XML encode: %v", err)
	}

	os.Stdout.Write(output)
}
