// Mlf2resp monitors the AMQP message broker for command responses from an
// MLF2 float and stores the responses in the CouchDB database.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/messages"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/mq"
	"github.com/BurntSushi/toml"
	uuid "github.com/satori/go.uuid"
	"github.com/streadway/amqp"
)

const (
	VIEW       = "messaging/pending"
	EMAILQUEUE = "email_reply"
)

type mqConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Vhost    string `json:"vhost,omitempty" toml:"vhost,omitempty"`
	Exchange string `json:"exchange" toml:"exchange"`
}

type dbConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Dbname   string `json:"dbname" toml:"dbname"`
}

type sysConfig struct {
	Mq mqConfig `json:"mq" toml:"mq"`
	Db dbConfig `json:"db" toml:"db"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
mlf2resp [options] configfile

Monitor the AMQP message broker for command responses from an MLF2 float
and store the responses in the CouchDB database.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false, "Enable debugging output")
)

func (cfg mqConfig) Uri() string {
	return fmt.Sprintf("amqp://%s:%s@%s/%s",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Vhost)
}

func (cfg dbConfig) Uri() string {
	return fmt.Sprintf("http://%s:%s@%s/",
		cfg.User,
		cfg.Password,
		cfg.Host)
}

func findResponse(db *couchdb.Database, key string) (messages.Response, error) {
	var opts = map[string]interface{}{
		"key": key,
	}

	resp := messages.Response{}
	rows, err := db.Query(couchdb.NewView(VIEW), opts)
	if err != nil {
		return resp, err
	}

	if rows.Next() {
		if err = rows.ScanValue(&resp); err != nil {
			return resp, err
		}
	}

	return resp, nil
}

// reloadMessage makes a new copy of a messages.Message and stores it in the
// database with its state set to "ready"
func reloadMessage(db *couchdb.Database, m_src *messages.Message) error {
	m := m_src.Clone()
	m.ID = uuid.NewV4().String()
	_, err := db.Put(m.ID, m)
	return err
}

// sendReply publishes an email version of the float response.
func sendReply(pr *mq.Producer, resp messages.Response, comment string) error {
	headers := make(amqp.Table)
	headers["floatid"] = int32(resp.Floatid)
	headers["comment"] = comment

	msg := amqp.Publishing{
		Headers:      headers,
		ContentType:  "text/plain",
		DeliveryMode: 2,
		ReplyTo:      resp.Notify,
		Timestamp:    time.Now(),
		Body:         []byte(resp.String()),
	}

	return pr.Publish(EMAILQUEUE, msg)
}

// handleMessage processes a response from an MLF2 float as follows:
//
//    - extract the corresponding message ID from the header
//    - use the ID to retrieve the message document from the database
//    - use the ID to retrieve the partial response document if it exists
//      otherwise create a new one.
//    - append the float response to the response document
//    - if the EOT header is present, the response is complete. Set the message
//      document state to 'sent' and save the timestamp to the response
//      document.
func handleMessage(db *couchdb.Database, pr *mq.Producer, deliveries <-chan amqp.Delivery) {
	var (
		err  error
		resp messages.Response
	)

	for d := range deliveries {
		db_id, ok := d.Headers["db_id"].(string)
		if !ok {
			log.Printf("No database ID header")
			d.Reject(false)
			continue
		}

		m := messages.Message{}
		if err := db.Get(db_id, &m); err != nil {
			log.Printf("Cannot fetch message: %q", db_id)
			d.Reject(false)
			continue
		}

		// Check the database for a partial response
		resp, err = findResponse(db, db_id)
		if err != nil {
			log.Println(err)
			resp = messages.Response{}
		}

		// Create a new Response struct if necessary
		if resp.ID == "" {
			var fid int32
			resp.Type = "response"
			fid, ok = d.Headers["floatid"].(int32)
			resp.Floatid = int(fid)
			resp.Notify = d.ReplyTo
			resp.Ref = db_id
			resp.ID = uuid.NewV4().String()
			resp.Contents = make([]messages.CommandReply, 0)
		}

		// Unmarshal the command sent to the float and the corresponding
		// reply from the message body.
		reply := messages.CommandReply{}
		if err = json.Unmarshal(d.Body, &reply); err == nil {
			resp.Contents = append(resp.Contents, reply)
		}

		// Is this the final response to a Message?
		if _, ok = d.Headers["eot"].(string); ok {
			log.Printf("Message delivery complete: %q", db_id)
			ts := time.Now().UTC()
			resp.Received = messages.Time{ts}
			m.State = "sent"
			m.Ref = resp.ID
			m.Delivered = messages.Time{ts}
			if err = sendReply(pr, resp, m.Comment); err != nil {
				log.Printf("sendReply error: %v", err)
			}
			if m.Sticky {
				if err = reloadMessage(db, &m); err != nil {
					log.Printf("Cannot reload message %q: %v", m.ID, err)
				}
			}
		} else {
			m.State = "delivery"
		}

		// Update the response
		if _, err = db.Put(resp.ID, resp); err != nil {
			log.Printf("Cannot store response %q: %v", resp.ID, err)
		} else {
			d.Ack(false)
		}

		// Update the message
		if _, err = db.Put(m.ID, m); err != nil {
			log.Printf("Cannot store message %q: %v", m.ID, err)
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("read config file: %v", err)
	}

	if err = toml.Unmarshal(b, &cfg); err != nil {
		log.Fatalf("parse config file: %v", err)
	}

	db := couchdb.NewDatabase(cfg.Db.Uri(), cfg.Db.Dbname)
	if db == nil {
		log.Fatalf("Invalid database URL: %q", cfg.Db.Uri())
	}

	consumer, err := mq.NewConsumer(cfg.Mq.Uri(), cfg.Mq.Exchange, "topic")
	if err != nil {
		log.Fatalf("AMPQ connection: %v", err)
	}

	producer, err := mq.NewProducer(cfg.Mq.Uri(), "", "")
	if err != nil {
		log.Fatalf("AMPQ connection: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	dh := func(ch <-chan amqp.Delivery) {
		handleMessage(db, producer, ch)
	}

	err = consumer.Start("replymon", "responses", "*.responses", dh)
	if err != nil {
		log.Fatalf("Message consumer: %v", err)
	}

	err_ch := consumer.CloseNotifier()

	// Shutdown the consumer on a signal interupt or AMQP connection close
	go func() {
		select {
		case s := <-sigs:
			log.Printf("Got signal: %v", s)
		case err := <-err_ch:
			log.Printf("AMQP error: %#v", err)
		}
		consumer.Shutdown()
	}()

	// Wait for the consumer to exit
	consumer.Wait()
}
