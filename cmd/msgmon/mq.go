package main

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/messages"
	"github.com/pkg/errors"
	"github.com/rs/xid"
	"github.com/streadway/amqp"
)

func mqSetup(cfg mqConfig) (*amqp.Channel, error) {
	conn, err := amqp.Dial(cfg.Uri())
	if err != nil {
		return nil, errors.Wrap(err, "connect")
	}

	channel, err := conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "open channel")
	}

	if err = channel.ExchangeDeclare(
		cfg.Exchange, // name of the exchange
		"topic",      // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return nil, errors.Wrap(err, "exchange declare")
	}

	return channel, nil
}

func bindQueue(ch *amqp.Channel, exchange, qname string) error {
	_, err := ch.QueueDeclare(
		qname, // name of the queue
		true,  // durable
		false, // delete when usused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "queue declare")
	}

	if err = ch.QueueBind(
		qname,    // name of the queue
		qname,    // bindingKey
		exchange, // sourceExchange
		false,    // noWait
		nil,      // arguments
	); err != nil {
		return errors.Wrap(err, "queue bind")
	}

	return nil
}

func publishCommands(m messages.Message, ch *amqp.Channel, exchange string) error {
	headers := make(amqp.Table)
	headers["floatid"] = int32(m.Floatid)
	headers["db_id"] = m.ID
	headers["comment"] = m.Comment

	id := xid.New()
	key := fmt.Sprintf("float-%d.commands", m.Floatid)
	if err := bindQueue(ch, exchange, key); err != nil {
		return err
	}

	n := m.Count()
	for i, cmd := range m.Commands() {
		if i == (n - 1) {
			headers["eot"] = "yes"
		}
		headers["uuid"] = xid.New().String()
		msg := amqp.Publishing{
			Headers:       headers,
			ContentType:   "text/plain",
			DeliveryMode:  2,
			CorrelationId: id.String(),
			ReplyTo:       m.Sender,
			Timestamp:     time.Now(),
			Body:          []byte(cmd.String()),
		}

		if err := ch.Publish(exchange, key, false, false, msg); err != nil {
			return err
		}
	}

	return nil
}

func cancelCommands(m messages.Message, ch *amqp.Channel, exchange string) error {
	n, err := delMatch(ch, m.Floatid, m.ID)
	if err != nil {
		// Republish the cancellation message
		log.Printf("Message removal failed: %v", err)

		key := fmt.Sprintf("float-%d.cancellation", m.Floatid)
		if err := bindQueue(ch, exchange, key); err != nil {
			return err
		}

		text := fmt.Sprintf("[%q]", m.ID)
		msg := amqp.Publishing{
			ContentType:  "application/json",
			DeliveryMode: 2,
			ReplyTo:      m.Sender,
			Timestamp:    time.Now(),
			Body:         []byte(text),
		}
		err = ch.Publish(exchange, key, false, false, msg)
	} else {
		log.Printf("%d messages removed from queue", n)
	}

	return err
}

func delMatch(ch *amqp.Channel, floatid int, id string) (int, error) {
	qname := fmt.Sprintf("float-%d.commands", floatid)
	qinfo, err := ch.QueueInspect(qname)
	if err != nil {
		return 0, err
	}

	msgs := make([]amqp.Delivery, 0)
	count := int(0)
	var cmd_id, msg_id string

	for i := 0; i < qinfo.Messages; i++ {
		msg, ok, err := ch.Get(qname, false)
		if !ok || err != nil {
			return count, err
		}

		if val, found := msg.Headers["uuid"]; found {
			cmd_id, _ = val.(string)
		} else {
			cmd_id = ""
		}

		if val, found := msg.Headers["db_id"]; found {
			msg_id, _ = val.(string)
		} else {
			msg_id = msg.CorrelationId
		}

		if msg_id == id || cmd_id == id {
			// ACK the message to remove it from the queue
			msg.Ack(false)
			count++
		} else {
			msgs = append(msgs, msg)
		}

	}

	// Requeue messages
	for _, msg := range msgs {
		msg.Reject(true)
	}

	return count, nil
}
