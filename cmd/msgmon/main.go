// Msgmon monitors the MLF2 CouchDB database for new messages and
// publishes them to an AMQP message broker.
package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/messages"
	"github.com/BurntSushi/toml"
	"github.com/streadway/amqp"
)

type mqConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Vhost    string `json:"vhost,omitempty" toml:"vhost,omitempty"`
	Exchange string `json:"exchange" toml:"exchange"`
}

type dbConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Dbname   string `json:"dbname" toml:"dbname"`
	Filter   string `json:"filter" toml:"filter"`
}

type sysConfig struct {
	Mq mqConfig `json:"mq" toml:"mq"`
	Db dbConfig `json:"db" toml:"db"`
}

type dbMsg struct {
	Seq string
	Msg messages.Message
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `msgmon [options] configfile

Monitor the MLF2 database for new message entries and publish the messages
to the AMQP message queue for the corresponding float.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug  = flag.Bool("debug", false, "Enable debugging output")
	dryrun = flag.Bool("dry-run", false,
		"Monitor changes but do not process messages")
	seqFile = flag.String("seq", "", "File to store CouchDB update sequence number")
)

func (cfg mqConfig) Uri() string {
	return fmt.Sprintf("amqp://%s:%s@%s/%s",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Vhost)
}

func (cfg dbConfig) Uri() string {
	return fmt.Sprintf("http://%s:%s@%s/",
		cfg.User,
		cfg.Password,
		cfg.Host)
}

func processMessage(m messages.Message, ch *amqp.Channel, exchange string,
	db *couchdb.Database) error {
	var err error

	switch m.State {
	case "cancelled", "canceled":
		return cancelCommands(m, ch, exchange)
	case "ready":
		// Publish the Message to the message broker and, if successful,
		// update the state to "queued"
		if err = publishCommands(m, ch, exchange); err != nil {
			return err
		} else {
			m.State = "queued"
			m.Queued = messages.Time{time.Now().UTC()}
			_, err = db.Put(m.ID, m)
		}
	}

	return err
}

func saveSeq(filename, val string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = fmt.Fprintln(f, val)
	return err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("read config file: %v", err)
	}

	if err = toml.Unmarshal(b, &cfg); err != nil {
		log.Fatalf("parse config file: %v", err)
	}

	db := couchdb.NewDatabase(cfg.Db.Uri(), cfg.Db.Dbname)
	if db == nil {
		log.Fatalf("Invalid database URL: %q", cfg.Db.Uri())
	}

	mqch, err := mqSetup(cfg.Mq)
	if err != nil {
		log.Fatal(err)
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var (
		lastSeq string
	)

	if *seqFile != "" {
		f, err := os.Open(*seqFile)
		if err != nil {
			log.Printf("WARNING Cannot open seq# file: %v", err)
		} else {
			fmt.Fscanln(f, &lastSeq)
			f.Close()
		}
	}

	if lastSeq == "" {
		log.Println("Looking up last sequence number")
		stats, err := db.Info()
		if err != nil {
			log.Fatalf("Cannot access CouchDB: %v", err)
		}
		switch v := stats["update_seq"].(type) {
		case float64:
			lastSeq = strconv.Itoa(int(v))
		case string:
			lastSeq = v
		}
	}

	v := url.Values{}
	v.Set("filter", cfg.Db.Filter)
	v.Set("feed", "continuous")
	v.Set("heartbeat", "5000")
	v.Set("since", lastSeq)
	v.Set("include_docs", "true")

	// Start a goroutine to receive the DB changes information, download
	// the corresponding Message and write it to a channel.
	log.Println("Starting DB monitor")
	ch := make(chan dbMsg, 1)
	go func() {
		defer close(ch)
		changes, err := db.Changes(ctx, v)
		if err != nil {
			return
		}

		for change := range changes {
			if *debug {
				log.Printf("Document change: %s", change.Id)
			}
			m := dbMsg{}
			m.Seq = change.SeqString()
			err = change.ScanDoc(&m.Msg)
			if err != nil {
				log.Printf("Doc scan error: %v", err)
			} else {
				select {
				case ch <- m:
				default:
					log.Println("WARNING: message queue full")
				}
			}
		}
	}()

loop:
	for {
		select {
		case m, more := <-ch:
			if !more {
				break loop
			}
			if *dryrun {
				log.Printf("Message: %#v", m.Msg)
			} else {
				if err = processMessage(m.Msg, mqch, cfg.Mq.Exchange, db); err != nil {
					log.Printf("processMessage: %v", err)
				} else {
					lastSeq = m.Seq
				}
			}
		case s := <-sigs:
			log.Printf("Got signal: %v", s)
			cancel()
			break loop
		}
	}

	if *seqFile != "" {
		log.Printf("Saving last seq (%v) to %s", lastSeq, *seqFile)
		err = saveSeq(*seqFile, lastSeq)
		if err != nil {
			log.Println(err)
		}
	}
}
