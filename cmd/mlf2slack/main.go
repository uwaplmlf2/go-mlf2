// Mlf2slack publishes MLF2 Status messages to a Slack Channel.
package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/mq"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/status"
	"github.com/BurntSushi/toml"
	"github.com/streadway/amqp"
)

type slackConfig struct {
	Url string `json:"url" toml:"url"`
}

type mqConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Vhost    string `json:"vhost,omitempty" toml:"vhost,omitempty"`
	Exchange string `json:"exchange,omitempty" toml:"exchange,omitempty"`
}

type sysConfig struct {
	Mq    mqConfig    `json:"mq" toml:"mq"`
	Slack slackConfig `json:"slack" toml:"slack"`
}

type slackMessage struct {
	Text string `json:"text"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
mlf2slack [options] configfile

Listen on an AMQP queue for MLF2 status messages and publish them to a Slack channel.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug   = flag.Bool("debug", false, "Enable debugging output")
	bindkey = flag.String("binding-key", "*.status.processed", "queue binding key")
	exclude = flag.String("exclude", "", "exclude the named float group")
	dryrun  = flag.Bool("dry-run", false, "don't send Tweets")
)

func (cfg mqConfig) Uri() string {
	return fmt.Sprintf("amqp://%s:%s@%s/%s",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Vhost)
}

func formatMessage(floatid int, s *status.Status) slackMessage {
	lat := status.NewDmm(s.Gps.Lat)
	lon := status.NewDmm(s.Gps.Lon)

	var hlat, hlon rune
	if lat.IsNeg() {
		hlat = 'S'
	} else {
		hlat = 'N'
	}

	if lon.IsNeg() {
		hlon = 'W'
	} else {
		hlon = 'E'
	}

	text := []string{fmt.Sprintf("<!channel> mlf2_%d %s%c %s%c %d\n%s",
		floatid,
		lat.Abs(), hlat,
		lon.Abs(), hlon,
		s.Gps.Nsat,
		s.Timestamp)}

	return slackMessage{Text: strings.Join(append(text, s.Alerts...), "\n")}
}

func handleMessage(webhook string, deliveries <-chan amqp.Delivery) {
	for d := range deliveries {
		floatid, ok := d.Headers["floatid"].(int32)
		if !ok {
			floatid = 0
		}
		group, ok := d.Headers["group"].(string)
		if !ok {
			group = ""
		}

		if *exclude != "" && group == *exclude {
			log.Printf("Skipping message from float %d (group %q)", floatid, group)
			d.Ack(true)
			continue
		}

		var (
			s   status.Status
			err error
		)

		if d.ContentType == "application/xml" {
			err = xml.Unmarshal(d.Body, &s)
		} else {
			// JSON
			err = json.Unmarshal(d.Body, &s)
		}

		if err != nil {
			log.Printf("Cannot decode message: %v", err)
			d.Nack(false, false)
			continue
		}

		msg := formatMessage(int(floatid), &s)
		if *dryrun {
			log.Printf("Message: %#v", msg)
			d.Ack(true)
		} else {
			payload, _ := json.Marshal(msg)
			tr := &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			}
			client := &http.Client{Transport: tr}
			_, err = client.Post(webhook, "application/json", bytes.NewBuffer(payload))
			if err != nil {
				d.Reject(true)
				log.Printf("http POST error: %v", err)
			} else {
				d.Ack(true)
			}
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("read config file: %v", err)
	}

	if err = toml.Unmarshal(b, &cfg); err != nil {
		log.Fatalf("parse config file: %v", err)
	}

	if cfg.Mq.Vhost == "" {
		cfg.Mq.Vhost = "mlf2"
	}

	if cfg.Mq.Exchange == "" {
		cfg.Mq.Exchange = "data"
	}

	consumer, err := mq.NewConsumer(cfg.Mq.Uri(), cfg.Mq.Exchange, "topic")
	if err != nil {
		log.Fatalf("AMQP connection: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	dh := func(ch <-chan amqp.Delivery) {
		handleMessage(cfg.Slack.Url, ch)
	}

	err = consumer.Start("slack", "", *bindkey, dh)
	if err != nil {
		log.Fatalf("Message consumer: %v", err)
	}

	err_ch := consumer.CloseNotifier()

	// Shutdown the consumer on a signal interupt or AMQP connection close
	go func() {
		select {
		case s := <-sigs:
			log.Printf("Got signal: %v", s)
		case err := <-err_ch:
			log.Printf("AMQP error: %#v", err)
		}
		consumer.Shutdown()
	}()

	// Wait for the consumer to exit
	consumer.Wait()
}
