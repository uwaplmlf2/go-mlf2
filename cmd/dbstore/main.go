// Dbstore monitors the MLF2 message queues and stores the contents in a CouchDB
// database.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/mq"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/status"
	"github.com/BurntSushi/toml"
	"github.com/streadway/amqp"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
dbstore [options] configfile

Monitor the AMQP broker for data messages from MLF2 floats and store them
in the CouchDB database.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
)

// Process an MLF2 data file from the message broker
func handleFile(db *couchdb.Database, pr *mq.Producer, ch <-chan amqp.Delivery) {
	var (
		id       string
		err      error
		filename string
		float_id int32
		ok       bool
	)

	services := map[string]string{
		"application/x-netcdf": "env_plot",
		"text/x-sexp":          "sexp_convert",
	}

	for d := range ch {
		if errmsg, ok := d.Headers["error"].(string); ok {
			d.Reject(false)
			log.Printf("File processing error: %q", errmsg)
			continue
		}

		if float_id, ok = d.Headers["floatid"].(int32); !ok {
			d.Reject(false)
			log.Println("no float ID specified")
			continue
		}

		if filename, ok = d.Headers["filename"].(string); !ok {
			d.Reject(false)
			log.Println("no file name specified")
			continue
		}

		if db_id, ok := d.Headers["db_id"].(string); !ok {
			// New file
			id, err = storeFile(int(float_id), filename, db, &d)
			if err != nil {
				d.Reject(false)
				log.Printf("Cannot store %q: %v", filename, err)
				continue
			}

			// Some file types are sent to RPC services for further processing
			if service := services[d.ContentType]; service != "" {
				qkey := fmt.Sprintf("float-%d.file.processed", float_id)
				d.Headers["db_id"] = id
				msg := amqp.Publishing{
					Headers:       d.Headers,
					ContentType:   d.ContentType,
					Timestamp:     time.Now().UTC(),
					ReplyTo:       qkey,
					CorrelationId: id,
					Body:          d.Body,
				}
				if err = pr.Publish(service, msg); err != nil {
					log.Printf("Cannot publish to %q: %v", service, err)
				}
			}

		} else {
			// Attachment for an existing file
			_, err = addAttachment(filename, db_id, db, &d)
			if err != nil {
				d.Reject(false)
				log.Printf("Cannot store %q: %v", filename, err)
				continue
			}
			id = db_id
		}

		log.Printf("%s (%s) from float %d, ID = %s", filename, d.ContentType, float_id, id)
		d.Ack(false)

	}
}

// Process an MLF2 status message from the message broker
func handleStatus(db *couchdb.Database, pr *mq.Producer, ch <-chan amqp.Delivery) {
	var (
		qname    string
		float_id int32
		ok       bool
	)

	for d := range ch {
		if float_id, ok = d.Headers["floatid"].(int32); !ok {
			d.Reject(false)
			log.Println("no float ID specified")
			continue
		}

		float, err := getFloat(db, int(float_id))
		if err != nil {
			d.Reject(true)
			log.Printf("Unknown float ID: %v", err)
			continue
		}

		doc, err := storeStatus(int(float_id), db, &d)
		if err != nil {
			d.Reject(false)
			log.Printf("Cannot store status record: %v", err)
			continue
		}

		log.Printf("Status message from float %d, ID = %s", float_id, doc.ID)
		float.Lastlogin = d.Timestamp.Format(status.TimeFormat)
		d.Ack(false)
		_, err = db.Put(float.ID, float)
		if err != nil {
			log.Printf("Cannot update float record: %v", err)
		}

		// Publish the processed status message
		if b, err := json.Marshal(doc); err == nil {
			d.Headers["db_id"] = doc.ID
			d.Headers["group"] = float.Group
			if float.Group != "" {
				qname = float.Group + ".status.processed"
			} else {
				qname = fmt.Sprintf("float-%d.status.processed", float_id)
			}
			msg := amqp.Publishing{
				Headers:     d.Headers,
				ContentType: "application/json",
				Timestamp:   time.Now().UTC(),
				Body:        b,
			}
			if err = pr.Publish(qname, msg); err != nil {
				log.Printf("Cannot publish to %q: %v", qname, err)
			}
		} else {
			log.Printf("JSON marshal error: %v", err)
		}
	}

}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("read config file: %v", err)
	}

	if err = toml.Unmarshal(b, &cfg); err != nil {
		log.Fatalf("parse config file: %v", err)
	}

	db := couchdb.NewDatabase(cfg.Db.Uri(), cfg.Db.Dbname)
	if db == nil {
		log.Fatalf("Invalid database URL: %q", cfg.Db.Uri())
	}

	consumer, err := mq.NewConsumer(cfg.Mq.Uri(), cfg.Mq.Exchange, "topic")
	if err != nil {
		log.Fatalf("AMPQ connection: %v", err)
	}

	producer, err := mq.NewProducer(cfg.Mq.Uri(), cfg.Mq.Exchange, "topic")
	if err != nil {
		log.Fatalf("AMPQ connection: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	err = consumer.Start("statusmon", "float.status",
		"*.status",
		func(ch <-chan amqp.Delivery) {
			handleStatus(db, producer, ch)
		})
	if err != nil {
		log.Fatalf("Status consumer: %v", err)
	}

	err = consumer.Start("filemon", "float.file",
		"*.file",
		func(ch <-chan amqp.Delivery) {
			handleFile(db, producer, ch)
		})
	if err != nil {
		log.Fatalf("File consumer: %v", err)
	}

	err = consumer.Start("attachments", "file.processed",
		"*.file.processed",
		func(ch <-chan amqp.Delivery) {
			handleFile(db, producer, ch)
		})
	if err != nil {
		log.Fatalf("File consumer: %v", err)
	}

	err_ch := consumer.CloseNotifier()

	// Shutdown the consumer on a signal interupt or AMQP connection close
	go func() {
		select {
		case s := <-sigs:
			log.Printf("Got signal: %v", s)
		case err := <-err_ch:
			log.Printf("AMQP error: %#v", err)
		}
		consumer.Shutdown()
	}()

	// Wait for the consumer to exit
	consumer.Wait()
}
