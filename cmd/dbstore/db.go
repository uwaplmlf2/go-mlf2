package main

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/uwaplmlf2/couchdb"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/status"
	"github.com/dsnet/golib/memfile"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"github.com/streadway/amqp"
)

// MLF2 float entry in the database
type Float struct {
	ID        string                        `json:"_id,omitempty"`
	Rev       string                        `json:"_rev,omitempty"`
	Floatid   int                           `json:"floatid"`
	Group     string                        `json:"group"`
	Imei      string                        `json:"imei"`
	Lastlogin string                        `json:"last_login"`
	Type      string                        `json:"type"`
	Att       map[string]couchdb.Attachment `json:"_attachments,omitempty"`
}

// Extend memfile.File to support the io.ReadCloser interface
type memoryFile struct {
	*memfile.File
}

func newMemoryFile(b []byte) *memoryFile {
	m := &memoryFile{}
	m.File = memfile.New(b)
	return m
}

func (m *memoryFile) Close() error {
	return nil
}

// Retrieve a float description from the database
func getFloat(db *couchdb.Database, floatid int) (Float, error) {
	id := fmt.Sprintf("apl.uw.mlf2:%d", floatid)
	f := Float{}
	err := db.Get(id, &f)
	f.Type = "active"
	return f, err
}

// Store a file in the database. The database entry is a JSON document
// describing the file with the file contents included as an
// attachment.
func storeFile(float_id int, filename string, db *couchdb.Database,
	msg *amqp.Delivery) (string, error) {

	var (
		err     error
		doc     files.CouchDbMarshaler
		id, rev string
	)

	// Extract the document from the message body
	doc, err = getDocument(msg, filename)
	if err != nil {
		return id, err
	}

	// Generate a unique document ID
	id = uuid.NewV4().String()

	// Metadata to add to each document
	md := []files.Metadata{
		{Key: "floatid", Value: float_id},
		{Key: "filename", Value: filename},
		{Key: "type", Value: "data"},
		{Key: "t_recv", Value: msg.Timestamp.Unix()},
	}

	// Marshal into a CouchDB JSON document
	cdoc, err := doc.MarshalCouchDb(id, "", md...)
	if err != nil {
		return id, errors.Wrap(err, "couchdb marshal")
	}

	// Insert or update
	rev, err = db.Put(id, json.RawMessage(cdoc))
	if err != nil {
		return id, fmt.Errorf("PUT %s: %w", id, err)
	}

	// Add the file contents as an attachment
	att := couchdb.Attachment{
		Filename:    filename,
		ContentType: msg.ContentType,
		Content:     newMemoryFile(msg.Body),
		Length:      len(msg.Body),
	}

	_, err = db.PutAttachment(id, rev, att)
	if err != nil {
		return id, fmt.Errorf("PUT attachment %s/%s: %w",
			id, att.Filename, err)
	}

	return id, nil
}

// Add the contents of an AMQP message to an existing database document as an attachment.
func addAttachment(filename, id string, db *couchdb.Database, msg *amqp.Delivery) (string, error) {

	var (
		rev string
	)

	rev, err := db.GetRev(id)
	if err != nil {
		return rev, fmt.Errorf("Revision lookup %s: %w", id, err)
	}

	att := couchdb.Attachment{
		Filename:    filename,
		ContentType: msg.ContentType,
		Content:     newMemoryFile(msg.Body),
		Length:      -1,
	}
	return db.PutAttachment(id, rev, att)
}

func storeStatus(float_id int, db *couchdb.Database, msg *amqp.Delivery) (*status.Status, error) {
	var (
		err error
		doc files.CouchDbMarshaler
	)

	// Extract the document from the message body
	doc, err = getStatus(msg)
	if err != nil {
		return nil, err
	}

	id := uuid.NewV4().String()

	// Metadata to add to each document
	md := []files.Metadata{
		{Key: "floatid", Value: float_id},
		{Key: "type", Value: "status"},
		{Key: "t_recv", Value: msg.Timestamp.Unix()},
	}

	// Marshal into a CouchDB JSON document
	cdoc, err := doc.MarshalCouchDb(id, "", md...)
	if err != nil {
		return nil, errors.Wrap(err, "couchdb marshal")
	}

	// Insert or update
	_, err = db.Put(id, json.RawMessage(cdoc))
	if err != nil {
		return nil, fmt.Errorf("PUT %s: %w", id, err)
	}

	return doc.(*status.Status), nil
}
