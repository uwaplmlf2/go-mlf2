package main

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"encoding/xml"
	"strconv"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/csv"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/netcdf"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/sexp"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/status"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/syslog"
	tm "bitbucket.org/uwaplmlf2/go-mlf2/pkg/time"
	"github.com/dsnet/golib/memfile"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

// Time format in directory list file
const ListingTimeFormat = "2006-01-02 15:04:05"

// photoTimestamp creates a timestamp from the components of a photo file name
func photoTimestamp(filename string, ref time.Time) tm.Timestamp {
	yd, _ := strconv.Atoi(filename[1:4])
	h, _ := strconv.Atoi(filename[4:6])
	m, _ := strconv.Atoi(filename[6:8])
	minutes := (((yd-1)*24+h)*60 + m)

	var ystart time.Time

	if ref.YearDay() >= yd {
		// Photo is from the reference year
		ystart = time.Date(ref.Year(), time.January, 1, 0, 0, 0, 0, time.UTC)
	} else {
		// Photo is from the previous year
		ystart = time.Date(ref.Year()-1, time.January, 1, 0, 0, 0, 0, time.UTC)
	}

	ts := ystart.Add(time.Duration(minutes) * time.Minute)
	return tm.Timestamp{ts}
}

// getDocument creates a CouchDB document from the contents of an AMQP message.
// If the contents are gzip compressed, they will be uncompressed by this
// function.
func getDocument(msg *amqp.Delivery, filename string) (files.CouchDbMarshaler, error) {
	var (
		doc files.CouchDbMarshaler
		err error
	)

	if msg.ContentEncoding == "gzip" {
		b := bytes.NewBuffer(msg.Body)
		zr, err := gzip.NewReader(b)
		if err != nil {
			return nil, errors.Wrap(err, "gzip reader")
		}
		zr.Multistream(false)
		out := new(bytes.Buffer)
		if _, err = out.ReadFrom(zr); err != nil {
			return nil, errors.Wrap(err, "gunzip")
		}

		// Replace message body with uncompressed content
		msg.Body = out.Bytes()
		msg.ContentEncoding = ""
	}

	f := memfile.New(msg.Body)
	switch msg.ContentType {
	case "application/x-netcdf":
		doc, err = netcdf.Summarize(f)
		if err != nil {
			return nil, errors.Wrap(err, "netcdf read")
		}
	case "text/x-sexp":
		doc, err = sexp.Summarize(f)
		if err != nil {
			return nil, errors.Wrap(err, "sexp read")
		}
	case "text/csv":
		doc, err = csv.Summarize(f)
		if err != nil {
			return nil, errors.Wrap(err, "csv read")
		}
	case "text/x-syslog":
		doc, err = syslog.Summarize(f)
		if err != nil {
			return nil, errors.Wrap(err, "csv read")
		}
	case "image/jpeg":
		// Photo image timestamp is incorporated in the filename
		s := &files.Summary{}
		s.Tstart = photoTimestamp(filename, time.Now().UTC())
		s.Tstop = s.Tstart
		s.Filetype = "image"
		doc = s
	case "text/x-listing":
		// Directory listing contains a timestamp on the first line
		if i := bytes.IndexByte(msg.Body, '\n'); i != -1 {
			var ts time.Time
			ts, err = time.Parse(ListingTimeFormat, string(msg.Body[0:i]))
			if err != nil {
				ts = time.Now()
			}
			s := &files.Summary{}
			s.Tstart = tm.Timestamp{ts.UTC()}
			s.Tstop = s.Tstart
			s.Filetype = "listing"
			doc = s
		} else {
			return nil, errors.New("invalid listing format")
		}
	default:
		doc, _ = files.Summarize(f)
	}

	return doc, nil
}

func getStatus(msg *amqp.Delivery) (files.CouchDbMarshaler, error) {
	var (
		s   status.Status
		err error
	)

	switch msg.ContentType {
	case "application/xml":
		if err = xml.Unmarshal(msg.Body, &s); err != nil {
			return nil, errors.Wrap(err, "xml decode")
		}
	case "application/json":
		if err = json.Unmarshal(msg.Body, &s); err != nil {
			return nil, errors.Wrap(err, "json decode")
		}
	default:
		return nil, errors.New("unsupported content type")
	}

	return files.CouchDbMarshaler(&s), nil
}
