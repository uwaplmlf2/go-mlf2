package main

import (
	"encoding/json"
	"io/ioutil"
	"testing"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/files"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/netcdf"
	tm "bitbucket.org/uwaplmlf2/go-mlf2/pkg/time"
	uuid "github.com/satori/go.uuid"
	"github.com/streadway/amqp"
)

//const FILENAME = "env0316.nc.gz"
const FILENAME = "env0260.ncz"
const LISTING = `2018-01-18 06:00:46
SYSLOG02.TXT      28553
BAL00001.TXT        864
COMMTEST.RUN     147096
DROGUE.SCM         1327
IOTEST.RUN        74400
MENU.SCM           2733
`

func TestGetDocument(t *testing.T) {
	ncfiles := []string{
		"test.ncz",
		"telem.ncz",
		"env0260.ncz",
	}

	for _, name := range ncfiles {
		b, err := ioutil.ReadFile("testdata/" + name)
		if err != nil {
			t.Fatalf("Cannot read file: %v", err)
		}

		d := amqp.Delivery{
			ContentType:     "application/x-netcdf",
			ContentEncoding: "gzip",
			Body:            b,
		}

		size := len(b)
		doc, err := getDocument(&d, "")
		if err != nil {
			t.Fatal(err)
		}

		_, ok := doc.(*netcdf.Summary)
		if !ok {
			t.Errorf("Expected netcdf.Summary; got %T", doc)
		}

		if d.ContentEncoding != "" || len(d.Body) <= size {
			t.Error("File contents not uncompressed")
		}

		cdoc, err := doc.MarshalCouchDb("someRandomId", "")
		if err != nil {
			t.Errorf("Cannot create JSON document: %v", err)
		}

		partial := struct {
			Id  string `json:"_id"`
			Rev string `json:"_rev"`
		}{}
		err = json.Unmarshal(cdoc, &partial)
		if partial.Rev != "" {
			t.Errorf("Bad _rev; expected '', got %q", partial.Rev)
		}
	}

}

func TestGetListing(t *testing.T) {
	d := amqp.Delivery{
		ContentType: "text/x-listing",
		Body:        []byte(LISTING),
	}

	doc, err := getDocument(&d, "")
	if err != nil {
		t.Fatal(err)
	}

	dd, ok := doc.(*files.Summary)
	if !ok {
		t.Errorf("Expected files.Summary; got %T", doc)
	}

	if dd.Tstart.Unix() != 1516255246 {
		t.Errorf("Bad timestamp: expected 1516255246; got %d", dd.Tstart.Unix())
	}
}

func TestCouchdbMarshal(t *testing.T) {
	d := amqp.Delivery{
		ContentType: "text/x-listing",
		Body:        []byte(LISTING),
	}

	doc, err := getDocument(&d, "")
	if err != nil {
		t.Fatal(err)
	}

	// Generate a unique document ID
	id := uuid.NewV4().String()

	// Metadata to add to each document
	md := []files.Metadata{
		{Key: "floatid", Value: 42},
		{Key: "filename", Value: "00index"},
		{Key: "type", Value: "data"},
	}

	// Marshal into a CouchDB JSON document
	cdoc, err := doc.MarshalCouchDb(id, "", md...)
	b, err := json.Marshal(json.RawMessage(cdoc))
	if err != nil {
		t.Fatal(err)
	}

	if string(cdoc) != string(b) {
		t.Errorf("Wrong encoding; expected %q, got %q", string(cdoc), string(b))
	}
}

func TestPhotoTimestamp(t *testing.T) {
	tt := []struct {
		name    string
		reftime time.Time
		expect  tm.Timestamp
	}{
		{
			name:    "I2491659.jpg",
			reftime: time.Date(2015, time.October, 1, 0, 0, 0, 0, time.UTC),
			expect:  tm.Timestamp{time.Date(2015, time.September, 6, 16, 59, 0, 0, time.UTC)},
		},
		{
			name:    "s3641659.jpg",
			reftime: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC),
			expect:  tm.Timestamp{time.Date(2015, time.December, 30, 16, 59, 0, 0, time.UTC)},
		},
		{
			name:    "M3641659.jpg",
			reftime: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC),
			expect:  tm.Timestamp{time.Date(2015, time.December, 30, 16, 59, 0, 0, time.UTC)},
		},
	}

	for _, test := range tt {
		ts := photoTimestamp(test.name, test.reftime)
		if !ts.Equal(test.expect) {
			t.Errorf("Got %v; expected %v", ts, test.expect)
		}
	}

}
