package main

import "fmt"

type mqConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Vhost    string `json:"vhost,omitempty" toml:"vhost,omitempty"`
	Exchange string `json:"exchange" toml:"exchange"`
}

type dbConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Dbname   string `json:"dbname" toml:"dbname"`
}

type sysConfig struct {
	Mq mqConfig `json:"mq" toml:"mq"`
	Db dbConfig `json:"db" toml:"db"`
}

func (cfg mqConfig) Uri() string {
	return fmt.Sprintf("amqp://%s:%s@%s/%s",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Vhost)
}

func (cfg dbConfig) Uri() string {
	return fmt.Sprintf("http://%s:%s@%s/",
		cfg.User,
		cfg.Password,
		cfg.Host)
}
