// Mlf2tweet publishes MLF2 status information on Twitter.
package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/mq"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/status"
	"github.com/BurntSushi/toml"
	"github.com/ChimeraCoder/anaconda"
	"github.com/streadway/amqp"
)

type twitterCreds struct {
	ConsumerKey    string `json:"consumer_key" toml:"consumerKey"`
	ConsumerSecret string `json:"consumer_secret" toml:"consumerSecret"`
	AuthToken      string `json:"auth_token" toml:"authToken"`
	AuthSecret     string `json:"auth_secret" toml:"authSecret"`
}

type mqConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Vhost    string `json:"vhost,omitempty" toml:"vhost,omitempty"`
	Exchange string `json:"exchange,omitempty" toml:"exchange,omitempty"`
}

type sysConfig struct {
	Mq      mqConfig     `json:"mq" toml:"mq"`
	Twitter twitterCreds `json:"twitter" toml:"twitter"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
mlf2tweet [options] configfile

Listen on an AMQP queue for MLF2 status messages and publish them to Twitter.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug   = flag.Bool("debug", false, "Enable debugging output")
	bindkey = flag.String("binding-key", "*.status.processed", "queue binding key")
	exclude = flag.String("exclude", "", "exclude the named float group")
	dryrun  = flag.Bool("dry-run", false, "don't send Tweets")
)

func (cfg mqConfig) Uri() string {
	return fmt.Sprintf("amqp://%s:%s@%s/%s",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Vhost)
}

func accessApi(creds twitterCreds) *anaconda.TwitterApi {
	anaconda.SetConsumerKey(creds.ConsumerKey)
	anaconda.SetConsumerSecret(creds.ConsumerSecret)
	return anaconda.NewTwitterApi(creds.AuthToken, creds.AuthSecret)
}

func formatTweet(floatid int, s *status.Status) string {
	lat := status.NewDmm(s.Gps.Lat)
	lon := status.NewDmm(s.Gps.Lon)

	var hlat, hlon rune
	if lat.IsNeg() {
		hlat = 'S'
	} else {
		hlat = 'N'
	}

	if lon.IsNeg() {
		hlon = 'W'
	} else {
		hlon = 'E'
	}

	text := []string{fmt.Sprintf("#mlf2_%d %s%c %s%c %d %s",
		floatid,
		lat.Abs(), hlat,
		lon.Abs(), hlon,
		s.Gps.Nsat,
		s.Timestamp.Format("2006-01-02T15:04:05-07:00"))}

	return strings.Join(append(text, s.Alerts...), "\n")
}

func handleMessage(api *anaconda.TwitterApi, deliveries <-chan amqp.Delivery) {
	for d := range deliveries {
		floatid, ok := d.Headers["floatid"].(int32)
		if !ok {
			floatid = 0
		}
		group, ok := d.Headers["group"].(string)
		if !ok {
			group = ""
		}

		if *exclude != "" && group == *exclude {
			log.Printf("Skipping message from float %d (group %q)", floatid, group)
			d.Ack(true)
			continue
		}

		var (
			s   status.Status
			err error
		)

		if d.ContentType == "application/xml" {
			err = xml.Unmarshal(d.Body, &s)
		} else {
			// JSON
			err = json.Unmarshal(d.Body, &s)
		}

		if err != nil {
			log.Printf("Cannot decode message: %v", err)
			d.Nack(false, false)
			continue
		}

		msg := formatTweet(int(floatid), &s)
		if *dryrun {
			log.Printf("Tweet: %q", msg)
			d.Ack(true)
		} else {
			v := url.Values{}
			v.Set("lat", fmt.Sprintf("%.6f", s.Gps.Lat))
			v.Set("long", fmt.Sprintf("%.6f", s.Gps.Lon))
			_, err = api.PostTweet(msg, v)
			if err != nil {
				d.Reject(true)
				log.Printf("Tweet error: %v", err)
			} else {
				d.Ack(true)
			}
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("read config file: %v", err)
	}

	if err = toml.Unmarshal(b, &cfg); err != nil {
		log.Fatalf("parse config file: %v", err)
	}

	if cfg.Mq.Vhost == "" {
		cfg.Mq.Vhost = "mlf2"
	}

	if cfg.Mq.Exchange == "" {
		cfg.Mq.Exchange = "data"
	}

	api := accessApi(cfg.Twitter)
	consumer, err := mq.NewConsumer(cfg.Mq.Uri(), cfg.Mq.Exchange, "topic")
	if err != nil {
		log.Fatalf("AMQP connection: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	dh := func(ch <-chan amqp.Delivery) {
		handleMessage(api, ch)
	}

	err = consumer.Start("tweety", "", *bindkey, dh)
	if err != nil {
		log.Fatalf("Message consumer: %v", err)
	}

	// Shutdown the AMQP consumer on a signal interupt
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			consumer.Shutdown()
		}
	}()

	// Wait for the consumer to exit
	consumer.Wait()
}
