#!/usr/bin/env bash

while read unit rest; do
    systemctl disable --now $unit
done < <(systemctl list-units --no-legend 'mlf2data-*.service')

container=$(portablectl list --no-legend | grep attached | cut -f1 -d' ')
if [[ -n $container ]]; then
    portablectl detach $container
fi
