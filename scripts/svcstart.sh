#!/usr/bin/env bash

version="$(cat .version)"
container="mlf2data_${version}.raw"
[[ -f $container ]] || {
    echo "Container image $container not found"
    exit 1
}

set -e
sudo cp -v "$container" /data/portables
sudo portablectl attach "/data/portables/$container"

while read unit rest; do
    systemctl enable --now $unit
done < <(systemctl list-unit-files --no-legend 'mlf2data-*.service')
